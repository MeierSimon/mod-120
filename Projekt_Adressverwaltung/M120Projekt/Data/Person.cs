﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace M120Projekt.Data
{
    public class Person
    {
        #region Datenbankschicht
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 PeronenId { get; set; }
        [Required]
        public String Vorname { get; set; }
        [Required]
        public String Nachname { get; set; }
        [Required]
        public String Strasse { get; set; }
        [Required]
        public int Hausnummer { get; set; }
        [Required]
        public int Postleitzahl { get; set; }
        [Required]
        public String Ort { get; set; }
        
        public String Telefonnummer { get; set; }
        
        public String Email { get; set; }
        [Required]
        public Boolean IstAktiv { get; set; }
        #endregion
        #region Applikationsschicht
        public Person() 
        {
            
        }
        [NotMapped]
        public String BerechnetesAttribut
        {
            get
            {
                return "Im Getter kann Code eingefügt werden für berechnete Attribute";
            }
        }
        public static List<Person> LesenAlle()
        {
            using (var db = new Context())
            {
                return (from record in db.Person select record).ToList();
            }
        }
        public static Person LesenID(Int64 klasseAId)
        {
            using (var db = new Context())
            {
                return (from record in db.Person where record.PeronenId == klasseAId select record).FirstOrDefault();
            }
        }
        public static List<Person> LesenAttributGleich(String suchbegriff)
        {
            using (var db = new Context())
            {
                return (from record in db.Person where record.Vorname == suchbegriff select record).ToList();
            }
        }
        public static List<Person> LesenAttributWie(String suchbegriff)
        {
            using (var db = new Context())
            {
                return (from record in db.Person where record.Vorname.Contains(suchbegriff) select record).ToList();
            }
        }
        public Int64 Erstellen()
        {
            var existingEntry = LesenID(this.PeronenId);
            if (existingEntry != null)
            {
                throw new ArgumentException("Person mit dieser Id existiert bereits.");
            }
            if (string.IsNullOrEmpty(this.Vorname))
            {
                throw new ArgumentException("Vorname ist leer. Darf nicht leer sein!");
            }
            if (string.IsNullOrEmpty(this.Nachname))
            {
                throw new ArgumentException("Nachname ist leer. Darf nicht leer sein!");
            }
            if (string.IsNullOrEmpty(this.Strasse))
            {
                throw new ArgumentException("Strasse ist leer. Darf nicht leer sein!");
            }
            if (string.IsNullOrEmpty(this.Ort))
            {
                throw new ArgumentException("Ort ist leer. Darf nicht leer sein!");
            }
            using (var db = new Context())
            {
                db.Person.Add(this);
                db.SaveChanges();
                return this.PeronenId;
            }
        }
        public Int64 Aktualisieren()
        {
            using (var db = new Context())
            {
                db.Entry(this).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return this.PeronenId;
            }
        }
        public void Loeschen()
        {
            using (var db = new Context())
            {
                db.Entry(this).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            }
        }
        public override string ToString()
        {
            return PeronenId.ToString(); // Für bessere Coded UI Test Erkennung
        }
        #endregion
    }
}
