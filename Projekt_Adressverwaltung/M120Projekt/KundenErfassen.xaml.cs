﻿using System;
using System.CodeDom;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;


namespace M120Projekt
{
    /// <summary>
    /// Interaction logic for KundenErfassen.xaml
    /// </summary>
    public partial class KundenErfassen : UserControl
    {

        private bool istEmailValide { get; set; }
        private bool istVornameValide { get; set; }
        private bool istNameValide { get; set; }
        private bool istStrasseValide { get; set; }
        private bool istOrtValide { get; set; }
        private bool istHausnummerValide { get; set; }
        private bool istPlzValide { get; set; }
        private bool istTelefonValide { get; set; }
        public KundenErfassen()
        {
            InitializeComponent();

            Speichern.IsEnabled = false;
        }

        private void KundeSpeichern_Click(object sender, RoutedEventArgs e)
        {
            Data.Person person = new Data.Person();
            person.Vorname = Vorame.Text;
            person.Nachname = Name.Text;
            person.Strasse = Strasse.Text;
            person.Hausnummer = Int32.Parse(Hausnummer.Text);
            person.Postleitzahl = Int32.Parse(Postleitzahl.Text);
            person.Ort = Ort.Text;
            person.IstAktiv = true;
            person.Telefonnummer = Telefonnummer.Text;
            person.Email = Email.Text;

            person.Erstellen();

            Speichern.IsEnabled = false;

            Vorame.Clear();
            Name.Clear();
            Strasse.Clear();
            Hausnummer.Clear();
            Postleitzahl.Clear();
            Ort.Clear();
            Telefonnummer.Clear();
            Email.Clear();

            istNameValide = false;
            istVornameValide = false;
            istStrasseValide = false;
            istHausnummerValide = false;
            istPlzValide = false;
            istOrtValide = false;
            istTelefonValide = false;
            istEmailValide = false;
        }

        private void Abbrechen_Click(object sender, RoutedEventArgs e)
        {
            Vorame.Clear();
            Name.Clear();
            Strasse.Clear();
            Hausnummer.Clear();
            Postleitzahl.Clear();
            Ort.Clear();
            Telefonnummer.Clear();
            Email.Clear();

            Speichern.IsEnabled = false;

            istNameValide = false;
            istVornameValide = false;
            istStrasseValide = false;
            istHausnummerValide = false;
            istPlzValide = false;
            istOrtValide = false;
            istTelefonValide = false;
            istEmailValide = false;
        }

        public void IstSpeichernEnalbled()
        {
            if (istEmailValide && istNameValide && istVornameValide && istStrasseValide && istHausnummerValide && istPlzValide && istOrtValide && istTelefonValide)
            {
                Speichern.IsEnabled = true;
            }
            else
            {
                Speichern.IsEnabled = false;
            }
        }
        
        private void Email_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Text.RegularExpressions.Regex mailRegex = new System.Text.RegularExpressions.Regex(@"([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}");

            System.Text.RegularExpressions.Match match = mailRegex.Match(Email.Text);

            istEmailValide = match.Success;
            if (!istEmailValide)
            {
                Email.Background = Brushes.LightCoral;
                
            }
            else
            {
                Email.Background = Brushes.LightGreen;
                
            }

            IstSpeichernEnalbled();
        }

        private void Vorame_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Text.RegularExpressions.Regex vorNameRegex = new System.Text.RegularExpressions.Regex(@"\p{L}");

            System.Text.RegularExpressions.Match match = vorNameRegex.Match(Vorame.Text);
            istVornameValide = match.Success;
            if (!istVornameValide)
            {
                Vorame.Background = Brushes.LightCoral;
            }
            else
            {
                Vorame.Background = Brushes.LightGreen;
            }

            IstSpeichernEnalbled();

        }
        private void Name_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Text.RegularExpressions.Regex nameRegex = new System.Text.RegularExpressions.Regex(@"\p{L}");

            System.Text.RegularExpressions.Match match = nameRegex.Match(Name.Text);
            istNameValide = match.Success;
            if (!istNameValide)
            {
                Name.Background = Brushes.LightCoral;
            }
            else
            {
                Name.Background = Brushes.LightGreen;
            }

            IstSpeichernEnalbled();
        }
        private void Strasse_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Text.RegularExpressions.Regex nameRegex = new System.Text.RegularExpressions.Regex(@"\p{L}");

            System.Text.RegularExpressions.Match match = nameRegex.Match(Strasse.Text);
            istStrasseValide = match.Success;
            if (!istStrasseValide)
            {
                Strasse.Background = Brushes.LightCoral;
            }
            else
            {
                Strasse.Background = Brushes.LightGreen;
            }
            
            IstSpeichernEnalbled();
        }
        private void Ort_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Text.RegularExpressions.Regex nameRegex = new System.Text.RegularExpressions.Regex(@"\p{L}");

            System.Text.RegularExpressions.Match match = nameRegex.Match(Ort.Text);
            istOrtValide = match.Success;
            if (!istOrtValide)
            {
                Ort.Background = Brushes.LightCoral;
            }
            else
            {
                Ort.Background = Brushes.LightGreen;
            }
            
            IstSpeichernEnalbled();
        }
        private void Hausnummer_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Text.RegularExpressions.Regex nameRegex = new System.Text.RegularExpressions.Regex("[0-9]");

            System.Text.RegularExpressions.Match match = nameRegex.Match(Hausnummer.Text);
            istHausnummerValide = match.Success;
            if (!istHausnummerValide)
            {
                Hausnummer.Background = Brushes.LightCoral;
            }
            else
            {
                Hausnummer.Background = Brushes.LightGreen;
            }
            
            IstSpeichernEnalbled();
        }
        private void Postleitzahl_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Text.RegularExpressions.Regex nameRegex = new System.Text.RegularExpressions.Regex("[0-9]{4}");

            System.Text.RegularExpressions.Match match = nameRegex.Match(Postleitzahl.Text);
            istPlzValide = match.Success;
            if (!istPlzValide)
            {
                Postleitzahl.Background = Brushes.LightCoral;
            }
            else
            {
                Postleitzahl.Background = Brushes.LightGreen;
            }
            
            IstSpeichernEnalbled();
        }
        private void Telefonnummer_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Text.RegularExpressions.Regex nameRegex = new System.Text.RegularExpressions.Regex("[0-9]{9}");

            System.Text.RegularExpressions.Match match = nameRegex.Match(Telefonnummer.Text);
            istTelefonValide = match.Success;
            if (!istTelefonValide)
            {
                Telefonnummer.Background = Brushes.LightCoral;
            }
            else
            {
                Telefonnummer.Background = Brushes.LightGreen;
            }
            
            IstSpeichernEnalbled();
        }

        private void Vorame_LostFocus(object sender, RoutedEventArgs e)
        {
            Vorame.Background = Brushes.White;
        }

        private void Strasse_LostFocus(object sender, RoutedEventArgs e)
        {
            Strasse.Background = Brushes.White;
        }

        private void Hausnummer_LostFocus(object sender, RoutedEventArgs e)
        {
            Hausnummer.Background = Brushes.White;
        }

        private void Postleitzahl_LostFocus(object sender, RoutedEventArgs e)
        {
            Postleitzahl.Background = Brushes.White;
        }

        private void Ort_LostFocus(object sender, RoutedEventArgs e)
        {
            Ort.Background = Brushes.White;
        }

        private void Telefonnummer_LostFocus(object sender, RoutedEventArgs e)
        {
            Telefonnummer.Background = Brushes.White;
        }

        private void Email_LostFocus(object sender, RoutedEventArgs e)
        {
            Email.Background = Brushes.White;
        }

        private void Name_LostFocus(object sender, RoutedEventArgs e)
        {
            Name.Background = Brushes.White;
        }
    }
}
