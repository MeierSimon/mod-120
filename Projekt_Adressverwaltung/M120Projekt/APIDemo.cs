﻿using System;
using System.Diagnostics;

namespace M120Projekt
{
    static class APIDemo
    {
        #region Person
        // Create
        public static void DemoACreate()
        {
            Debug.Print("--- DemoACreate ---");
            // Person
            Data.Person person = new Data.Person();
            person.PeronenId = 1;
            person.Vorname = "Simon";
            person.Nachname = "Meier";
            person.Strasse = "Widenbach";
            person.Hausnummer = 3;
            person.Postleitzahl = 6246;
            person.Ort = "Altishofen";
            person.IstAktiv = true;
            
            Int64 personId = person.Erstellen();
            Debug.Print("Person erstellt mit Id:" + personId);
        }
        public static void DemoACreate2()
        {
            Data.Person person2 = new Data.Person();
            person2.PeronenId = 2;
            person2.Vorname = "Max";
            person2.Nachname = "Muster";
            person2.Strasse = "Musterstrasse";
            person2.Hausnummer = 42;
            person2.Postleitzahl = 6000;
            person2.Ort = "Musterhausen";
            person2.IstAktiv = true;
            
            Int64 person2Id = person2.Erstellen();
            Debug.Print("Person erstellt mit Id:" + person2Id);
        }

        // Read
        public static void DemoARead()
        {
            Debug.Print("--- DemoARead ---");
            // Demo liest alle
            foreach (Data.Person klasseA in Data.Person.LesenAlle())
            {
                Debug.Print("Artikel Id:" + klasseA.PeronenId + " Name:" + klasseA.Vorname);
            }
        }
        // Update
        public static void DemoAUpdate()
        {
            Debug.Print("--- DemoAUpdate ---");
            // Person ändert Attribute
            Data.Person klasseA1 = Data.Person.LesenID(1);
            klasseA1.Vorname = "Artikel 1 nach Update";
            klasseA1.Aktualisieren();
        }
        // Delete
        public static void DemoADelete()
        {
            Debug.Print("--- DemoADelete ---");
            Data.Person.LesenID(2).Loeschen();
            Debug.Print("Artikel mit Id 2 gelöscht");
        }
        #endregion
    }
}
