﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using M120Projekt.Data;

namespace M120Projekt
{
    /// <summary>
    /// Interaction logic for Kunden.xaml
    /// </summary>
    public partial class Kunden : UserControl
    {
        private ObservableCollection<Person> allePersonen { get; set; }

        public Kunden()
        {
            InitializeComponent();
            //this.DataContext = Personen;
            List<Person> personen = new List<Person>(Person.LesenAlle());
            allePersonen = new ObservableCollection<Person>();
            foreach (Person person in personen)
            {
                allePersonen.Add(person);
            }

            Persons.ItemsSource = allePersonen;
            
        }

        public void Bearbeiten_Click(object sender, RoutedEventArgs e)
        {
            Button label = (Button) sender;

            long id = ((Person) label.DataContext).PeronenId;
            
            Bearbeiten bearbeiten = new Bearbeiten(id);
            bearbeiten.Owner = Application.Current.MainWindow;
            bearbeiten.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            bearbeiten.ShowDialog();
            
        }

        public void Loeschen_Click(object sender, RoutedEventArgs e)
        {
            Button label = (Button) sender;

            long id = ((Person) label.DataContext).PeronenId;
            
            Loeschen loeschen = new Loeschen(id);
            loeschen.Owner = Application.Current.MainWindow;
            loeschen.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            loeschen.ShowDialog();
        }
    }
}
