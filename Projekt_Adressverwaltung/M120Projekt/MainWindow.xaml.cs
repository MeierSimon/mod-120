﻿using System;
using System.Windows;
using System.Windows.Threading;
using M120Projekt.Data;

namespace M120Projekt
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {             
        public MainWindow()
        {
            InitializeComponent();
                        
            
            try
            {
                using (var dbcontext = new Context())
                {
                    dbcontext.Database.Initialize(false);
                    dbcontext.Database.CreateIfNotExists();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("datenbank konnte nicht erstellt werden.", ex);
                
            }
        }
        private void KundeHinzufügen_Click(object sender, RoutedEventArgs e)
        {           
            KundenErfassen kundeErfassen = new KundenErfassen();
            UserAuswahl.Content = kundeErfassen;
        }

        private void KundenAnzeigen_Click(object sender, RoutedEventArgs e)
        {            
            Kunden kunden = new Kunden();
            UserAuswahl.Content = kunden;
        }

        public void Beenden_Click(object sender, RoutedEventArgs e)
        {         
            Beenden beenden = new Beenden();
            beenden.Owner = Application.Current.MainWindow;
            beenden.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            beenden.ShowDialog();
        }
    }
}
