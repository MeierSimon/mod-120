﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace M120Projekt
{
    /// <summary>
    /// Interaction logic for Loeschen.xaml
    /// </summary>
    public partial class Loeschen : Window
    {
        private long _personZumLöschen;

        public Loeschen(long person)
        {
            InitializeComponent();
            _personZumLöschen = person;
        }

        private void Abbrechen_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Loeschen_Click(object sender, RoutedEventArgs e)
        {
            Data.Person.LesenID(_personZumLöschen).Loeschen();

            this.Close();
        }
    }
}
